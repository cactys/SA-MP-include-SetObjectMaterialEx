// Set(Player)ObjectMaterial for RGBA Colors by Meta
// With this Include/code you can use RGBA Colors in Set(Player)ObjectMaterial

#include <a_samp>

stock RGBAtoARGB(color)
	return (color >>> 8)|(color << 24);
	
stock SetObjectMaterialEx(objectid, materialindex, modelid, txdname[], texturename[], materialcolor=0)
	return SetObjectMaterial(objectid, materialindex, modelid, txdname, texturename, RGBAtoARGB(materialcolor));
	
#if defined _ALS_SetObjectMaterial
	#undef SetObjectMaterial
#else
	#define _ALS_SetObjectMaterial
#endif
#define SetObjectMaterial SetObjectMaterialEx
	
stock SetPlayerObjectMaterialEx(playerid, objectid, materialindex, modelid, txdname[], texturename[], materialcolor=0)
	return SetPlayerObjectMaterial(playerid, objectid, materialindex, modelid, txdname, texturename, RGBAtoARGB(materialcolor));

#if defined _ALS_SetPlayerObjectMaterial
	#undef SetPlayerObjectMaterial
#else
	#define _ALS_SetPlayerObjectMaterial
#endif
#define SetPlayerObjectMaterial SetPlayerObjectMaterialEx

stock SetObjectMaterialTextEx(objectid, text[], materialindex = 0, materialsize = OBJECT_MATERIAL_SIZE_256x128, fontface[] = "Arial", fontsize = 24, bold = 1, fontcolor = 0xFFFFFFFF, backcolor = 0, textalignment = 0)
	return SetObjectMaterialText(objectid, text, materialindex, materialsize, fontface, fontsize, bold, RGBAtoARGB(fontcolor), RGBAtoARGB(backcolor), textalignment);

#if defined _ALS_SetObjectMaterialText
	#undef SetObjectMaterialText
#else
	#define _ALS_SetObjectMaterialText
#endif
#define SetObjectMaterialText SetObjectMaterialTextEx

stock SetPlayerObjectMaterialTextEx(playerid, objectid, text[], materialindex = 0, materialsize = OBJECT_MATERIAL_SIZE_256x128, fontface[] = "Arial", fontsize = 24, bold = 1, fontcolor = 0xFFFFFFFF, backcolor = 0, textalignment = 0)
	return SetPlayerObjectMaterialText(playerid, objectid, text, materialindex, materialsize, fontface, fontsize, bold, RGBAtoARGB(fontcolor), RGBAtoARGB(backcolor), textalignment);

#if defined _ALS_SetPlayerObjectMaterialTxt
	#undef SetPlayerObjectMaterialText
#else
	#define _ALS_SetPlayerObjectMaterialTxt
#endif
#define SetPlayerObjectMaterialText SetPlayerObjectMaterialTextEx
