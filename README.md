> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=332031)
> and was modified slightly to remove stuff like weird typos and expired
> links.

# <span style="color: #00BFFF">ObjectMaterial stuff for <span style="color: #FF0000">R</span><span style="color: #00FF00">G</span><span style="color: #00FF">B</span><span style="color: #7F7F7F">A</span> Colors</span>
> Now updated for RC7

Hey, this is just a simple Include to make the ObjectMaterial funtcions
work with RGBA colors like in client messages and everything else.

**<span style="color: red">Note</span>**: There is one disadvantage:  
You can't use it in timers anymore but you can fiddle around like this:
```c
// ...

SetTimerEx("CallSetObjectMaterial", 10000, 0, "dddssd", ...);

// ...

forward CallSetObjectMaterial(objectid, materialindex, modelid, txdname[], texturename[], materialcolor);
public CallSetObjectMaterial(objectid, materialindex, modelid, txdname[], texturename[], materialcolor)
{
	return SetObjectMaterial(objectid, materialindex, modelid, txdname, texturename, materialcolor);
}
```

Get it and include it to get started!

**EDIT (10th April 2012 - 22:02):**  
There was a big bug in the code that made the use of mixed colors
(yellow, pink ...) impossible. After some head-breaking hours of
thinking and help from from JaTochNietDan and Y_Less we three got a
solution (straight from Y_Less 😀 ENJOY IT!)

Now it 100% works and it is awesome!

## Credits
-	**Y_Less** (for the final solution)
-	**JaTochNietDan** (for some head-breaking hours and codes that were
	close to the solution)
-	**Hiddos** (for a hint towards ARGB)
